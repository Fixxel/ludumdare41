﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fish : MonoBehaviour {
	
	public List<Mesh> FishMeshL = new List<Mesh>();
	int meshNum;

	void Start () {
		meshNum = Random.Range(0, 3);
		GetComponent<MeshFilter>().mesh = FishMeshL[meshNum];

		GetComponent<Rigidbody>().velocity = new Vector3(0, Random.Range(30f, 45f), 0);
		transform.eulerAngles += new Vector3(Random.Range(-180, 180), Random.Range(-180, 180), Random.Range(-180, 180));

		GetComponent<Rigidbody>().drag = Random.Range(0.05f, 0.5f);
	}

	private void Update() {
		if(transform.position.y < - 5) Destroy(gameObject);
	}

	private void OnTriggerEnter(Collider other) {
		if(other.GetComponent<Blast>()){
			GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
			GetComponent<Rigidbody>().velocity = new Vector3(Random.Range(30f, 60f), Random.Range(-30f, 30f), Random.Range(-30f, 30f));

			GetComponent<MeshFilter>().mesh = FishMeshL[meshNum + 3];

			//scoring player and adding to combo
			FindObjectOfType<Score>().S += 100;
			FindObjectOfType<Score>().AddCombo();
		}
	}
}
