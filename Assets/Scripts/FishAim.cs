﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishAim : MonoBehaviour {
	
	public bool tryCatch;
	bool upOrDown;
	public float Speed;
	public GameObject Player;
	public GameObject FishTarget;
	public int fishCount;
	public int collisionCount;

	private void Start() {
		Speed = Random.Range(1f,5f);
	}

	void Update () {
		if(Speed > 15) Speed = 15;
		if(Speed < -15) Speed = -15;

		transform.Translate(upOrDown ? Vector3.up * Speed * Time.deltaTime : Vector3.down * Speed * Time.deltaTime);
	}

	private void OnTriggerEnter(Collider other) {
		collisionCount++;
	}

	private void OnTriggerStay(Collider other) {
		if(collisionCount == 2){
			if(tryCatch){
				fishCount++;

				//increase target speed
				FishTarget.GetComponent<FishTarget>().maxSpeed += Random.Range(0.15f,0.5f);
				FishTarget.GetComponent<FishTarget>().minSpeed += Random.Range(0.5f,1.15f);

				Speed = Random.Range(1f,5f) * fishCount;
				tryCatch = false;
			}
		}else if(collisionCount == 1){
			if(tryCatch){
				fishCount = 0;

				//Punish player slightly for missing to discourage spamming
				if(FindObjectOfType<Score>()){
					FindObjectOfType<Score>().S -= 5;
				}
				//Reset target speed
				FishTarget.GetComponent<FishTarget>().maxSpeed = 5f;
				FishTarget.GetComponent<FishTarget>().minSpeed = 1f;

				Speed = Random.Range(1f,5f);
				tryCatch = false;
			}
		}
	}

	private void OnTriggerExit(Collider other) {
		if(!other.GetComponent<FishTarget>()){
			upOrDown = !upOrDown;
		}
		collisionCount--;
	}
}
