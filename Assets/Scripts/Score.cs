﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Score : MonoBehaviour {
	public Canvas ComboCanvas;
	public Canvas HighscoreCanvas;
	public Canvas TimerCanvas;
	public GameObject StartCanvas;

	public GameObject Player;

	bool started;

	public int S;	//score
	private int HS; //high score
	public int Combo;
	public float ComboTimer;
	public float GameTimer;
	
	private void Start() {
		GetComponent<Text>().color = Color.black;
	}

	void Update () {
		if(Input.GetKeyDown(KeyCode.F) && !started){
			started = true;
			StartCanvas.SetActive(false);
		}

		GetComponent<Text>().text = "Score: " + S.ToString();
		ComboCanvas.GetComponent<Text>().text = "Combo " + Combo.ToString();

		if(Combo > 0) ComboTimer -= Time.deltaTime;
		if(ComboTimer <= 0) Combo = 0;

		if(started){
			GameTimer -= Time.deltaTime;
			TimerCanvas.GetComponent<Text>().text = Mathf.RoundToInt(GameTimer).ToString();
			if(GameTimer <= 0) TimeOver();
		}
	}

	public void AddCombo(){
		Combo++;

		if(Combo >= 2) S += 20 * Combo;

		ComboTimer = 5f;
	}

	private void TimeOver(){
		if(S > HS){
			HighscoreCanvas.GetComponent<Text>().text = "Highscore: " + S.ToString();
			HS = S;
		}
		
		S = 0;	//reset score
		GameTimer = 120; //reset timer
		started = false;
		Player.GetComponent<Player>().TimeOver();
		StartCanvas.SetActive(true);
	}
}
