﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishTarget : MonoBehaviour {

	bool upOrDown;
	float speed;
	public float maxSpeed;
	public float minSpeed;

	void Update () {
		speed = Random.Range(minSpeed, maxSpeed);
		transform.Translate(upOrDown ? Vector3.up * speed * Time.deltaTime : Vector3.down * speed * Time.deltaTime);
	}

	private void OnTriggerExit(Collider other) {
		if(!other.GetComponent<FishAim>()){
			upOrDown = !upOrDown;
		}
	}
}
