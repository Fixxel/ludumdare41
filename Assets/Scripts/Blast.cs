﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blast : MonoBehaviour {

	public float Speed;
	
	private void Start() {
		transform.position += new Vector3(1.5f,0,0);
		GetComponent<Rigidbody>().velocity = new Vector3(Speed, 0, 0);
	}
}
