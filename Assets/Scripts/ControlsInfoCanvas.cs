﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ControlsInfoCanvas : MonoBehaviour {

	public GameObject Player;

	void Update () {
		if(Player.GetComponent<Player>().BeatEmMode) GetComponent<Text>().text = "SPACE = attack, F = fish";
		else GetComponent<Text>().text = "W and S to control up/down";
	}
}
