﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

	public bool BeatEmMode;
	public GameObject Blast;
	public GameObject FishAim;
	public GameObject FishContainer;

	public int collisionCount;
	
	void Update () {
		//switching betweem fishing and beatem up mode
		if(Input.GetKeyDown(KeyCode.F)){
			if(collisionCount > 0){
				BeatEmMode = !BeatEmMode;	//can only change modes while on ground
				FishAim.GetComponent<FishAim>().collisionCount = 0;
				FishContainer.SetActive(BeatEmMode ? false : true);
			}

			if(FishAim.GetComponent<FishAim>().fishCount > 0){
				StartCoroutine(PullCatch(0.5f));
				GetComponentInChildren<Animator>().SetBool("IsJumping", true);
			}else if(FishAim.GetComponent<FishAim>().fishCount == 0){
				GetComponentInChildren<Animator>().SetBool("IsFishing", false);
				GetComponentInChildren<Animator>().SetBool("IsOnGround", true);
			}

			if(!BeatEmMode && collisionCount > 0){
				GetComponentInChildren<Animator>().SetBool("IsFishing", true);
				GetComponentInChildren<Animator>().SetBool("IsOnGround", false);
			}
		}

		//Fishing
		if(Input.GetKey(KeyCode.W) && !BeatEmMode){
			FishAim.GetComponent<FishAim>().Speed += FishAim.GetComponent<FishAim>().fishCount > 0 ? 0.1f * FishAim.GetComponent<FishAim>().fishCount : 0.1f;
		}else if(Input.GetKey(KeyCode.S) && !BeatEmMode){
			FishAim.GetComponent<FishAim>().Speed -= FishAim.GetComponent<FishAim>().fishCount > 0 ? 0.1f * FishAim.GetComponent<FishAim>().fishCount : 0.1f;
		}else if(Input.GetKeyDown(KeyCode.Space) && !BeatEmMode){
			FishAim.GetComponent<FishAim>().tryCatch = true;
		}

		//Beating
		if(Input.GetKeyDown(KeyCode.Space) && BeatEmMode && collisionCount == 0){
			GameObject I = Instantiate(Blast);
			I.transform.position = transform.position;
			Destroy(I, 3f);

			GetComponentInChildren<Animator>().SetBool("IsAttacking", true);
		}
		else if(Input.GetKeyUp(KeyCode.Space) && BeatEmMode && collisionCount == 0){
			GetComponentInChildren<Animator>().SetBool("IsAttacking", false);
		}

		if((Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.W)) && BeatEmMode){
			GetComponent<Rigidbody>().drag = 0f;
		}else if((Input.GetKeyUp(KeyCode.S) || Input.GetKeyUp(KeyCode.W)) && BeatEmMode){
			GetComponent<Rigidbody>().drag = 3.5f;
		}
	}

	private IEnumerator PullCatch(float forSeconds){

		yield return new WaitForSeconds(forSeconds);

		FishContainer.GetComponent<FishingContainer>().SpawnFishes();
		GetComponent<Rigidbody>().velocity = new Vector3(0, 100f, 0);
	}

	private void OnCollisionEnter(Collision other) {
		collisionCount++;

		if(!other.gameObject.GetComponent<ParticleSystem>()){
			GetComponentInChildren<Animator>().SetBool("IsOnGround", BeatEmMode ? true : false);

			//reset animation conditions
			GetComponentInChildren<Animator>().SetBool("IsJumping", false);
			GetComponentInChildren<Animator>().SetBool("IsFalling", false);
		}
	}
	private void OnCollisionExit(Collision other) {
		collisionCount--;
	}

	public void TimeOver(){
		BeatEmMode = true;
		FishAim.GetComponent<FishAim>().collisionCount = 0;
		FishAim.GetComponent<FishAim>().fishCount = 0;
		FishContainer.SetActive(BeatEmMode ? false : true);

		if(collisionCount > 0){
			GetComponentInChildren<Animator>().SetBool("IsFishing", false);
			GetComponentInChildren<Animator>().SetBool("IsOnGround", true);
		}
	}
}
