﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishesOnHook : MonoBehaviour {

	// Update is called once per frame
	void Update () {

		if(GetComponentInParent<FishAim>().fishCount < 1) GetComponent<TextMesh>().text = "Press SPACE to hook a fish!";
		if(GetComponentInParent<FishAim>().fishCount > 0) GetComponent<TextMesh>().text = GetComponentInParent<FishAim>().fishCount.ToString() + " Fish(es) on hook! Press F to pull!";
	}
}
