﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishingContainer : MonoBehaviour {
	
    public GameObject FishAim;
    public GameObject FishTarget;
    public GameObject Fish;
    public GameObject Score;

    public void SpawnFishes(){
        FishTarget.GetComponent<FishTarget>().maxSpeed = 5f;
        FishTarget.GetComponent<FishTarget>().minSpeed = 1f;
        FishAim.GetComponent<FishAim>().Speed = Random.Range(1f,5f);

        while(FishAim.GetComponent<FishAim>().fishCount > 0){
            GameObject I = Instantiate(Fish);
            I.transform.position = new Vector3(2.5f, 1f, 0);

            Score.GetComponent<Score>().S += 10;
            FishAim.GetComponent<FishAim>().fishCount--;
        }
        
    }
}
